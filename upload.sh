#!/bin/bash

s3cmd sync \
--acl-public \
--exclude "upload.sh" \
--exclude ".git" \
--exclude ".git*" \
--exclude "README.md" \
--default-mime-type="text/plain" \
. \
s3://public.dowdandassociates.com/logging-configuration/

