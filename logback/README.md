
Copy your desired logback.xml file to someplace in the classpath and name it `logback.xml` or `logback-test.xml` , e.g. `src/main/resources/logback.xml`

or set as a system property

```bash
java -Dlogback.configurationFile=/path/to/config.xml chapters.configuration.MyApp1
```

* [Reference](http://logback.qos.ch/manual/configuration.html)

### INFO, stdout

```
java \
-Dlog4j.configuration=https://bitbucket.org/edowd/logging-configuration/raw/master/blitz4j/log4j-info-stdout.properties \
-Dlogback.configurationFile=https://bitbucket.org/edowd/logging-configuration/raw/master/logback/logback-info-stdout.xml \
-Darchaius.configurationSource.additionalUrls=... \
-jar ...
```

### DEBUG, stdout

```
java \
-Dlog4j.configuration=https://bitbucket.org/edowd/logging-configuration/raw/master/blitz4j/log4j-debug-stdout.properties \
-Dlogback.configurationFile=https://bitbucket.org/edowd/logging-configuration/raw/master/logback/logback-debug-stdout.xml \
-Darchaius.configurationSource.additionalUrls=... \
-jar ...
```

## From public.dowdandassociates.com

### INFO, stdout

```
java \
-Dlog4j.configuration=http://public.dowdandassociates.com/logging-configuration/blitz4j/log4j-info-stdout.properties \
-Dlogback.configurationFile=http://public.dowdandassociates.com/logging-configuration/logback/logback-info-stdout.xml \
-Darchaius.configurationSource.additionalUrls=... \
-jar ...
```

### DEBUG, stdout

```
java \
-Dlog4j.configuration=http://public.dowdandassociates.com/logging-configuration/blitz4j/log4j-debug-stdout.properties \
-Dlogback.configurationFile=http://public.dowdandassociates.com/logging-configuration/logback/logback-debug-stdout.xml \
-Darchaius.configurationSource.additionalUrls=... \
-jar ...
```

